- analisar soluçao de problemas

## Regras

- 1 printar na tela de 1 a 100

- 2 toda vez que for divisível por 3 printar 'FOO'

- 3 \\ 5 printar 'BAR'

- 4 \\ por 3 e 5: 'FOOBAR' 
(EXCLUSIVO)

## Regras 2 string validate ou não valida

() [] {}

- sempre que abrir parenteses deve ser fechado
- pode estar uma dentro da outra, desde que abra e fecha uma dentro da outra
E não tenha intersecção

validas:
- 123(456)[]
- abc{1(def)}
não válidas:
- 123} 
- abc({)}
- def(