(1..100).each &lambda { |number| 

  return p 'FOOBAR' if number % 5 == 0 && number % 3 == 0

  return p 'FOO' if number % 3 == 0
  return p 'BAR' if number % 5 == 0

  p number 
}
