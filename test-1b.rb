(1..100).take(20).each &lambda { |number| 

  output = ''

  output << 'FOO' if number % 3 == 0
  output << 'BAR' if number % 5 == 0

  output = number.to_s if output == ''

  p output
}
