require 'rspec'

class String

  def is_valid?
    compiling_queue = [ ]

    closable_characters = {'(' => ')', '{' => '}', '[' => ']'}

    self.split('').each do |character|
      if(closable_characters.keys.include?(character))
        compiling_queue << closable_characters[character]
      end

      if(closable_characters.values.include?(character))
        popped_character = compiling_queue.last
          
        # if it entered inside here, there was a close, then there must have been an opening before.
        # Therefore if doesn't pop, we can safely assume the string is invalid
        compiling_queue.pop if popped_character == character or return false
      end
    end

    compiling_queue.empty?
  end

end

RSpec.describe 'string_validator' do
  
  it 'doesnt allow invalid strings to pass' do
    expect('123}'.is_valid?).to be false
    expect('abc({)}'.is_valid?).to be false
    expect('def('.is_valid?).to be false
    expect('}'.is_valid?).to eq false
    expect('(}}'.is_valid?).to eq false
  end

  it 'allows valid strings to pass' do
    expect('123(456)[]'.is_valid?).to eq true
    expect('abc{1(def)}'.is_valid?).to eq true
    expect('[(({([])}))]'.is_valid?).to eq true
  end
end